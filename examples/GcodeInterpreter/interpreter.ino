#include <EEPROM.h>
#include <UF_uArm.h>

UF_uArm uarm;
char data;

void setup(){
	Serial.begin(9600);
	Serial.print("Interpreter initiated\n");

	uarm.init();
	uarm.setServoSpeed(SERVO_R, 10);
	uarm.setServoSpeed(SERVO_L, 10);
	uarm.setServoSpeed(SERVO_ROT, 10);
}

void loop(){
	if(Serial.available()){
		data = Serial.read(); //read the data sent by converter.py
	}

	//extract the X Y Z E F values from data 
	//call setPosition() with extracted x y z

	//TODO: will need to implement a method in UF_uArm that also interprets E F
	//		and sends signals to appropriate motor(s)
}